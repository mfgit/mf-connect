/*jshint node:true, latedef:nofunc*/
'use strict';

// Gulp dependencies
var gulp = require('gulp');
var rename = require('gulp-rename');
var del = require('del');
var log = require('fancy-log');

// Build dependencies
var browserify = require('browserify');
var uglify = require('gulp-uglify');
var source = require('vinyl-source-stream');
var transform = require('vinyl-transform');
var through2 = require('through2');
var buffer = require('vinyl-buffer');

// Style dependencies
var less = require('gulp-less');
//var prefix = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var browsersync = require('browser-sync').create();

// Development dependencies
var jshint = require('gulp-jshint');

// Test dependencies
var mochaPhantomjs = require('gulp-mocha-phantomjs');

var sourceFile = './app-src/js/main.js';
var destFolder = './public/js';
var destFile = 'mf-connect.js';
var allJsFiles = './app-src/js/*.js';
var imagesPath = './app-src/img/**/*';
var openSansFonts = './app-src/fonts/**/*';
var bootstrapFonts =  './app-src/bower_components/bootstrap-sass/assets/fonts/**/*';

gulp.task('lint', function() {
	return gulp.src(allJsFiles)
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

gulp.task('lint-test', function() {
	return gulp.src('./test/**/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

gulp.task('test', ['lint-test'], function() {
    return gulp.src('test/client/index.html')
        .pipe(mochaPhantomjs());
});

gulp.task('browserify', function() {
	var b = browserify({
		entries: sourceFile
	});

	var bundle = function() {
		return b
			.bundle()
			.on('error', log.error)
			.pipe(source(destFile))
			.pipe(buffer())
			.pipe(gulp.dest(destFolder));
	};

	return bundle();
});

gulp.task('browserify-test', ['lint-test'], function() {
	var b = browserify({
		entries: 'test/app-src/index.js',
		insertGlobals: true,
		debug: true
	});

	var bundle = function() {
		return b
			.bundle()
			.on('error', log.error)
			.pipe(source('client-test.js'))
			.pipe(buffer())
			.pipe(gulp.dest('build'));
	};

	return bundle();
});

gulp.task('serve', ['copy', 'browserify', 'styles'], function() {
	browsersync.init({
		server: {
			baseDir: ['app-src', 'public']
		},
		port: 8000
	});

	gulp.watch(allJsFiles, ['watch-js']);
	gulp.watch('./app-src/scss/*.scss', ['watch-css']);
	gulp.watch('./app-src/*.html').on('change', browsersync.reload);
});

gulp.task('watch-js', ['browserify'], function(done) {
	log('JS change detected. Rebrowserify!');
	browsersync.reload();
	done();
});

gulp.task('watch-css', ['styles'], function (done) {
	log('SCSS change detected. Sass away!');
	browsersync.reload();
	done();
});

gulp.task('styles', ['clean-styles'], function() {
	return gulp
		.src('./app-src/scss/main.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(rename('mf-connect.css'))
		.pipe(gulp.dest('./build'))
		.pipe(gulp.dest('./public/styles'));
});

gulp.task('clean-styles', function(done) {
	var files = [].concat(
		'./build/*.css',
		'./public/styles/*.css'
	);
	clean(files, done);
});

gulp.task('minify', ['styles'], function() {
	return gulp.src('./build/mf-connect.css')
		.pipe(cleanCSS())
		.pipe(rename('mf-connect.min.css'))
		.pipe(gulp.dest('./public/styles'));
});

gulp.task('uglify', ['browserify'], function() {
    log('Uglifying mf-connect.js and renaming it to mf-connect.min.js');

	return gulp.src('./public/js/mf-connect.js')
		.pipe(uglify())
		.pipe(rename('mf-connect.min.js'))
		.pipe(gulp.dest(destFolder));
});

gulp.task('img', function() {
    return gulp.src(imagesPath)
        .pipe(gulp.dest('./public/img'));
});

gulp.task('fonts', function() {
    return gulp.src([openSansFonts, bootstrapFonts])
        .pipe(gulp.dest('./public/fonts'));
});

gulp.task('copy', ['fonts', 'img']);

/**
 * Delete all files in a given path
 * @param  {Array}   path - array of paths to delete
 * @param  {Function} done - callback when complete
 */
function clean(path, done) {
	del(path).then(function() {
		done();
	});
}

gulp.task('build', ['copy', 'uglify', 'minify']);

gulp.task('default', ['serve']);

module.exports = gulp;
